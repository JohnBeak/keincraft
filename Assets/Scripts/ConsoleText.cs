﻿using System.Collections;
using UnityEngine;

namespace Keincraft
{
    public class ConsoleText : MonoBehaviour
    {
        #region Data
        [SerializeField]
        private float timeout = 10f;
        #endregion

        #region Implementation
        private void Awake()
        {
            StartCoroutine("TimeoutTimer");
        }

        private IEnumerator TimeoutTimer()
        {
            float timeExpired = 0f;
            while (timeExpired < timeout)
            {
                timeExpired += Time.deltaTime;
                yield return null;
            }
            DestroyConsoleText();
        }

        private void DestroyConsoleText()
        {
            Destroy(gameObject);
        }
        #endregion
    }
}