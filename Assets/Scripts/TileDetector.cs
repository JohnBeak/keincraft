﻿using UnityEngine;

namespace Keincraft
{
    [RequireComponent(typeof(Collider))]
    public class TileDetector : MonoBehaviour
    {
        #region Data
        private Tile _tileBeingDetected;
        #endregion

        #region Interface
        public Tile TileBeingDetected
        {
            get
            {
                return _tileBeingDetected;
            }
        }
        #endregion

        #region Implementation
        private void OnTriggerEnter(Collider other)
        {
            _tileBeingDetected = other.GetComponent<Tile>();
            //Debug.Log(detectingTile + name + other.name);
        }

        private void OnTriggerExit(Collider other)
        {
            _tileBeingDetected = null;
            //Debug.Log(detectingTile + name + other.name);
        }
        #endregion
    }
}