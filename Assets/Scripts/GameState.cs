﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Keincraft
{
    [Serializable]
    public class GameState
    {
        #region Data
        [Serializable]
        public class SectorWrapper
        {
            #region Data
            public Vector2Int key;
            public SectorState value;
            #endregion

            #region Interface
            public SectorWrapper(Vector2Int _key, SectorState _value)
            {
                key = _key;
                value = _value;
            }
            #endregion
        }

        // Unity serializer doesn't support Dictionary; packing data using List
        public Dictionary<Vector2Int, SectorState> changedSectors;
        public List<SectorWrapper> changedSectorWrapper;
        public Vector3 playerPosition;
        public Quaternion playerRotation;
        public Quaternion cameraRotation;
        public Vector2Int currentSectorOffset;
        public Vector2Int seed;
        #endregion

        #region Interface
        #region Init
        public GameState(Vector2Int _seed)
        {
            changedSectors = new Dictionary<Vector2Int, SectorState>();
            playerPosition = Vector3.zero;
            playerRotation = Quaternion.identity;
            cameraRotation = Quaternion.identity;
            currentSectorOffset = Vector2Int.zero;
            seed = _seed;
        }
        #endregion

        #region ...
        public Vector2 SeedFloat
        {
            get
            {
                return new Vector2(ConvertIntSeedValueToFloat(seed.x), ConvertIntSeedValueToFloat(seed.y));
            }
        }

        private static float ConvertIntSeedValueToFloat(int intValue)
        {
            return (float)intValue / (float)int.MaxValue * 256f;
        }

        public SectorState GetSectorState(Vector2Int sectorOffset)
        {
            if (changedSectors == null)
            {
                return null;
            }

            if (!(changedSectors.ContainsKey(sectorOffset)))
            {
                return null;
            }

            return changedSectors[sectorOffset];
        }
        #endregion

        #region Serialization
        public void PackChangedSectorWrapper()
        {
            if (changedSectors == null)
            {
                changedSectors = new Dictionary<Vector2Int, SectorState>();
            }
            if (changedSectorWrapper == null)
            {
                changedSectorWrapper = new List<SectorWrapper>();
            }

            changedSectorWrapper.Clear();
            foreach (KeyValuePair<Vector2Int, SectorState> changedSectorKeyValuePair in changedSectors)
            {
                changedSectorKeyValuePair.Value.PackChangedTileWrapper();
                changedSectorWrapper.Add(new SectorWrapper(changedSectorKeyValuePair.Key, changedSectorKeyValuePair.Value));
            }
        }

        public void UnpackChangedSectorWrapper()
        {
            if (changedSectors == null)
            {
                changedSectors = new Dictionary<Vector2Int, SectorState>();
            }
            if (changedSectorWrapper == null)
            {
                changedSectorWrapper = new List<SectorWrapper>();
            }

            changedSectors.Clear();
            foreach (SectorWrapper sectorWrapper in changedSectorWrapper)
            {
                sectorWrapper.value.UnpackChangedTileWrapper();
                changedSectors.Add(sectorWrapper.key, sectorWrapper.value);
            }
        }
        #endregion
        #endregion
    }
}