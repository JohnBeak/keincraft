﻿using System.Collections;
using UnityEngine;

namespace Keincraft
{
    public class Tile : MonoBehaviour
    {
        #region Data
        private static GameLogic GameLogicReference
        {
            get
            {
                if (_gameLogicReference == null)
                {
                    _gameLogicReference = FindObjectOfType<GameLogic>();
                }
                return _gameLogicReference;
            }
        }
        private static GameLogic _gameLogicReference;

        private const int HeightLow = 8;
        private const int HeightHigh = 10;

        private TileType CurrentTileType
        {
            get
            {
                return _tileState.tileType;
            }
        }
        private int maxHealth;
        private float HealthPercent
        {
            get
            {
                return (float)_tileState.currentHealth / (float)maxHealth;
            }
        }
        private Vector3Int coords;
        private Renderer RendererReference
        {
            get
            {
                if (_rendererReference == null)
                {
                    _rendererReference = GetComponent<Renderer>();
                }
                return _rendererReference;
            }
        }
        private Renderer _rendererReference;
        private TileState _tileState = new TileState();
        private Sector SectorReference
        {
            get
            {
                if (_sectorReference == null)
                {
                    _sectorReference = GetComponentInParent<Sector>();
                }
                return _sectorReference;
            }
        }
        private Sector _sectorReference;
        #endregion

        #region Interface
        #region Public properties
        public string StatusText
        {
            get
            {
                return CurrentTileType.ToString() + " [" + coords.x + ", " + coords.y + ", " + coords.z + "]. Remaining health: " + _tileState.currentHealth;
            }
        }
        public bool TileStateIsChanged
        {
            get
            {
                return _tileState.isChanged;
            }
            private set
            {
                _tileState.isChanged = value;
            }
        }
        public TileState CurrentTileState
        {
            get
            {
                return _tileState;
            }
        }
        #endregion

        #region Dynamic generation
        public void UpdateCoords()
        {
            coords = new Vector3Int((int)Mathf.Round(transform.position.x), (int)Mathf.Round(transform.position.y), (int)Mathf.Round(transform.position.z));
        }

        public void SetTile(TileType newTileType, bool beingPlaced = false, TileState tileState = null)
        {
            bool beingLoaded = tileState != null;

            // set tile state
            if (beingLoaded)
            {
                _tileState = new TileState(tileState);
            }
            else
            {
                TileStateIsChanged |= beingPlaced || beingLoaded;
                _tileState.tileType = newTileType;
                _tileState.isPlaced = beingPlaced;
            }

            TileSettings tileSettings = GameLogicReference.GetTileSettings(newTileType);
            maxHealth = tileSettings.MaxHealth;
            if (_tileState.currentHealth <= 0 || beingPlaced || (!beingLoaded && !beingPlaced))
            {
                _tileState.currentHealth = maxHealth;
            }

            UpdateCoords();

            string textureName = "_MainTex";
            if (beingPlaced || (beingLoaded && tileState.isPlaced))
            {
                RendererReference.material.SetTexture(textureName, tileSettings.GetTexture(TileHeight.Bricks));
            }
            else
            {
                RendererReference.material.SetTexture(textureName, tileSettings.GetTexture(TileHeightFromSeedAndCoords()));
            }

            gameObject.layer = tileSettings.Layer;
            RendererReference.enabled = tileSettings.RendererEnabled;

            UpdateTextures();

            if (beingPlaced)
            {
                ReportCreated();
            }
        }

        public void SetTile(TileState tileState)
        {
            SetTile(tileState.tileType, false, tileState);
        }
        #endregion

        #region Player interaction
        public void ReceiveDamage(int damage)
        {
            ShowParticles();
            if (CurrentTileType == TileType.Air)
            {
                return;
            }
            TileStateIsChanged = true;
            _tileState.currentHealth -= damage;
            LogToConsole(CurrentTileType.ToString() + " [" + coords.x + ", " + coords.y + ", " + coords.z + "] received " + damage + " damage. Remaining health: " + _tileState.currentHealth);
            UpdateTextures();
            if (_tileState.currentHealth <= 0)
            {
                DestroyTile();
            }
        }
        #endregion

        #region UI
        public void ReportCreated()
        {
            LogToConsole(CurrentTileType.ToString() + " [" + coords.x + ", " + coords.y + ", " + coords.z + "] created. Health: " + _tileState.currentHealth);
        }
        #endregion
        #endregion

        #region Implementation
        #region Dynamic generation
        private void UpdateTextures()
        {
            // TODO
            // temporary solution
            RendererReference.material.color = new Color(HealthPercent, HealthPercent, HealthPercent, 1f);
        }

        private void DestroyTile()
        {
            LogToConsole("Destroying " + CurrentTileType.ToString() + " [" + coords.x + ", " + coords.y + ", " + coords.z + "]");
            SetTile(TileType.Air);
        }

        private TileHeight TileHeightFromSeedAndCoords()
        {
            // TODO: not actually using seed, lol
            Random.InitState((((coords.x) * 7 + (coords.z)) * 11 + coords.y) + 13);
            TileHeight tileHeight = TileHeight.Mid;
            if (coords.y < HeightLow + Random.Range(-3, 3))
            {
                tileHeight = TileHeight.Low;
            }
            else if (coords.y > HeightHigh + Random.Range(-3, 3))
            {
                tileHeight = TileHeight.High;
            }

            return tileHeight;
        }
        #endregion

        #region Player interaction
        private void ShowParticles()
        {
            // TODO
        }
        #endregion

        #region UI
        private void LogToConsole(string text)
        {
            GameLogicReference.LogToConsole(text);
        }
        #endregion
        #endregion
    }
}