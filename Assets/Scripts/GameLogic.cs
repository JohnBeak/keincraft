﻿using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace Keincraft
{
    public class GameLogic : MonoBehaviour
    {
        #region Data
        #region Physics
        [SerializeField]
        private float walkingSpeed = 3f / 32f;
        [SerializeField]
        private float jumpingSpeed = 3f / 32f;
        [SerializeField]
        private float gravity = 1f / 256f;
        private bool isGrounded;
        [SerializeField]
        private float terminalVelocity = 12f / 32f;
        private float verticalVelocity;
        #endregion

        #region Camera and Player
        [SerializeField]
        private float maxCameraAngle = 60f;
        [SerializeField]
        private float mouseSensitivity = 3f;
        [SerializeField]
        private Transform playerTransform;
        private CharacterController characterController;
        private Transform cameraTransform;
        private Camera playerCamera;
        #endregion

        #region Placing and breaking tiles
        [SerializeField]
        private Transform aimTargetNearTransform;
        [SerializeField]
        private Transform aimTargetFarTransform;
        [SerializeField]
        private TileDetector highlightTileDetector;
        [SerializeField]
        private TileDetector airTileDetector;
        [SerializeField]
        private TileDetector[] neighbouringTileDetectors;
        private bool DetectingNeighbouringTiles
        {
            get
            {
                for (int i = 0; i < neighbouringTileDetectors.Length; i++)
                {
                    if (neighbouringTileDetectors[i].TileBeingDetected != null)
                    {
                        return true;
                    }
                }
                return false;
            }
        }
        private bool CanBreakTile
        {
            get
            {
                return highlightTileDetector.TileBeingDetected != null;
            }
        }
        private bool CanPlaceTile
        {
            get
            {
                return airTileDetector.TileBeingDetected != null && DetectingNeighbouringTiles;
            }
        }
        [SerializeField]
        private GameObject[] tilePrefabs;
        private int currentTilePrefabIndex;
        [SerializeField]
        private GameObject previewTile;
        private MeshRenderer previewTileGraphicsMeshRenderer;
        private bool isPlacingMode;
        private int actionTimeout;
        [SerializeField]
        private TileSettings[] tileSettings;
        #endregion

        #region UI
        [SerializeField]
        private Text statusText;
        [SerializeField]
        private GameObject consoleTextPrefab;
        [SerializeField]
        private Transform consoleTextContainerTransform;
        [SerializeField]
        private Text modeText;
        #endregion

        #region Game State
        private Sector[,] sectors;
        private Vector2Int CurrentSectorOffset
        {
            get
            {
                return gameState.currentSectorOffset;
            }
            set
            {
                gameState.currentSectorOffset = value;
            }
        }
        private const float SectorSize = 8f;
        private const int ActionTimeoutLength = 6;
        private GameState gameState;
        private const string SavefilePath = "keincraft.sav";
        #endregion
        #endregion

        #region Interface
        #region UI
        public void LogToConsole(string text)
        {
            Text newConsoleText = Instantiate(consoleTextPrefab, consoleTextContainerTransform).GetComponent<Text>();
            newConsoleText.text = text;
        }
        #endregion

        #region Tiles
        public TileSettings GetTileSettings(TileType tileType)
        {
            int tileTypeIndex = (int)tileType;
            if (tileTypeIndex < tileSettings.Length)
            {
                return tileSettings[tileTypeIndex];
            }
            return null;
        }
        #endregion
        #endregion

        #region Implementation
        #region Init
        private void Awake()
        {
            verticalVelocity = 0f;
            characterController = playerTransform.GetComponent<CharacterController>();
            playerCamera = GetComponentInChildren<Camera>(); // TODO: handle missing camera
            cameraTransform = playerCamera.transform;
            Cursor.visible = false;
            previewTileGraphicsMeshRenderer = previewTile.GetComponentInChildren<MeshRenderer>();
            FindSectors();
            NewGame();
            isPlacingMode = true;
            UpdateHints();
            Cursor.lockState = CursorLockMode.Locked;
            actionTimeout = 0;
        }

        private void FindSectors()
        {
            Sector[] foundSectors = FindObjectsOfType<Sector>();
            int side = (int)Mathf.Sqrt(foundSectors.Length);
            sectors = new Sector[side, side];
            int foundSectorOffset = 0;
            for (int x = 0; x < side; x++)
            {
                for (int z = 0; z < side; z++)
                {
                    sectors[x, z] = foundSectors[foundSectorOffset];
                    foundSectorOffset++;
                }
            }
        }
        #endregion

        #region Update
        private void Update()
        {
            isGrounded = characterController.isGrounded;

            // change tile
            float mouseWheelDelta = Input.GetAxis("Mouse ScrollWheel");
            if (mouseWheelDelta > 0f)
            {
                currentTilePrefabIndex++;
                currentTilePrefabIndex %= tilePrefabs.Length;
            }
            else if (mouseWheelDelta < 0f)
            {
                currentTilePrefabIndex--;
                currentTilePrefabIndex += tilePrefabs.Length;
                currentTilePrefabIndex %= tilePrefabs.Length;
            }

            // preview tile
            Vector3 positionToSnap = aimTargetFarTransform.position;
            RaycastHit hitInfo;
            int layerMask = 1 << 9;
            Ray ray = playerCamera.ScreenPointToRay(new Vector3(playerCamera.pixelWidth / 2f, playerCamera.pixelHeight / 2f, 0f));
            if (Physics.Raycast(ray, out hitInfo, 5f, layerMask))
            {
                Vector3 hitVector = playerTransform.position - hitInfo.point;
                if (hitVector.magnitude < 2f)
                {
                    positionToSnap = aimTargetNearTransform.position;
                }
                else
                {
                    float directionFix = isPlacingMode ? 0.1f : -0.1f;
                    positionToSnap = hitInfo.point + hitVector.normalized * directionFix/**/;
                }
            }

            Vector3 snappedTargetPosition = new Vector3(Mathf.Round(positionToSnap.x), Mathf.Round(positionToSnap.y), Mathf.Round(positionToSnap.z));
            previewTile.transform.position = snappedTargetPosition;
            string previewTextureName = "_MainTex";
            Material prefabRendererMaterial = tilePrefabs[currentTilePrefabIndex].GetComponent<Renderer>().sharedMaterial;
            Texture prefabTexture = prefabRendererMaterial.GetTexture("_MainTex");
            if (CanBreakTile & !isPlacingMode)
            {
                previewTileGraphicsMeshRenderer.material.SetTexture(previewTextureName, null);
                previewTileGraphicsMeshRenderer.material.color = new Color(1f, 1f, 1f, 0.5f);

            }
            else if (CanPlaceTile & isPlacingMode)
            {
                previewTileGraphicsMeshRenderer.material.SetTexture(previewTextureName, prefabTexture);
                previewTileGraphicsMeshRenderer.material.color = new Color(1f, 1f, 1f, 0.5f);
            }
            else if (isPlacingMode)
            {
                previewTileGraphicsMeshRenderer.material.SetTexture(previewTextureName, prefabTexture);
                previewTileGraphicsMeshRenderer.material.color = new Color(1f, 0f, 0f, 0.5f);
            }
            else
            {
                previewTileGraphicsMeshRenderer.material.SetTexture(previewTextureName, null);
                previewTileGraphicsMeshRenderer.material.color = new Color(1f, 1f, 1f, 0f);
            }

            // status text
            if (CanBreakTile)
            {
                statusText.text = highlightTileDetector.TileBeingDetected.StatusText;
            }
            else
            {
                statusText.text = string.Empty;
            }
        }

        private void FixedUpdate()
        {
            // exit
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Application.Quit();
            }

            // new game
            if (Input.GetKeyDown(KeyCode.F5))
            {
                NewGame();
            }

            // load game
            else if (Input.GetKeyDown(KeyCode.F6))
            {
                LoadGame();
            }

            // save game
            else if (Input.GetKeyDown(KeyCode.F7))
            {
                SaveGame();
            }

            // walk
            float strafe = Input.GetAxis("Horizontal");
            float walk = Input.GetAxis("Vertical");

            Vector3 forward = playerTransform.forward;
            Vector3 movement = new Vector3(strafe * forward.z + walk * forward.x, 0f, walk * forward.z - strafe * forward.x);

            characterController.Move(movement * walkingSpeed);

            // check sector change
            Vector2Int newSectorOffset = new Vector2Int(Mathf.RoundToInt(playerTransform.position.x / SectorSize), Mathf.RoundToInt(playerTransform.position.z / SectorSize));
            if (newSectorOffset != CurrentSectorOffset)
            {
                ChangeSector(newSectorOffset);
            }

            // jump
            if (isGrounded)
            {
                bool jump = Input.GetButtonDown("Jump");

                if (jump)
                {
                    verticalVelocity = jumpingSpeed;
                }
            }

            gameState.playerPosition = playerTransform.position;

            // gravity
            characterController.Move(Vector3.up * verticalVelocity);
            verticalVelocity -= gravity;
            if (verticalVelocity < -terminalVelocity)
            {
                verticalVelocity = -terminalVelocity;
            }

            // mouse
            playerTransform.Rotate(Vector3.up, Input.GetAxis("Mouse X") * mouseSensitivity);
            gameState.playerRotation = playerTransform.localRotation;

            float oldCameraAngle = cameraTransform.localRotation.eulerAngles.x % 360f;
            if (oldCameraAngle > 180f)
            {
                oldCameraAngle -= 360f;
            }
            oldCameraAngle = Mathf.Clamp(oldCameraAngle, -maxCameraAngle, maxCameraAngle);
            float newCameraAngle = Mathf.Clamp(oldCameraAngle - Input.GetAxis("Mouse Y") * mouseSensitivity, -maxCameraAngle, maxCameraAngle);
            cameraTransform.Rotate(Vector3.left, oldCameraAngle - newCameraAngle);
            gameState.cameraRotation = cameraTransform.localRotation;

            // place and break
            if (actionTimeout > 0)
            {
                actionTimeout--;
            }
            if (Input.GetMouseButton(0))
            {
                if (actionTimeout <= 0)
                {
                    if (isPlacingMode)
                    {
                        if (CanPlaceTile)
                        {
                            airTileDetector.TileBeingDetected.SetTile((TileType)(currentTilePrefabIndex + 1), true); // TODO: replace crude indexing
                            actionTimeout = ActionTimeoutLength;
                        }
                    }
                    else
                    {
                        if (CanBreakTile)
                        {
                            highlightTileDetector.TileBeingDetected.ReceiveDamage(1);
                            actionTimeout = ActionTimeoutLength;
                        }
                    }
                }
            }

            // change mode
            if (Input.GetMouseButtonDown(1))
            {
                isPlacingMode = !isPlacingMode;
                UpdateHints();
            }
        }
        #endregion

        #region New Game, Loading, Saving
        private void PlacePlayer()
        {
            RaycastHit hitInfo;
            Ray ray = new Ray(new Vector3(0f, 128f, 0f), Vector3.down); // assuming that terrain is never higher than 128 (or even close)
            int layerMask = 1 << 9;
            if (Physics.Raycast(ray, out hitInfo, Mathf.Infinity, layerMask))
            {
                PlacePlayer(hitInfo.point + Vector3.up * 4f, Quaternion.identity, Quaternion.identity);
            }
        }

        private void PlacePlayer(Vector3 coords, Quaternion playerRotation, Quaternion cameraRotation)
        {
            playerTransform.position = coords;
            playerTransform.localRotation = playerRotation;
            cameraTransform.localRotation = cameraRotation;
        }

        private void NewGame()
        {
            Random.InitState((int)((Time.time % 1f) * (float)int.MaxValue));
            Vector2Int seed = new Vector2Int(Random.Range(int.MinValue, int.MaxValue), Random.Range(int.MinValue, int.MaxValue));
            gameState = new GameState(seed);
            ChangeSector(Vector2Int.zero, true);
            PlacePlayer();
            LogToConsole("Started new game. Seed: " + seed.x + ", " + seed.y);
        }

        private void LoadGame()
        {
            string gameStateSerialized = File.ReadAllText(SavefilePath);
            gameState = JsonUtility.FromJson<GameState>(gameStateSerialized);
            gameState.UnpackChangedSectorWrapper();
            ChangeSector(gameState.currentSectorOffset, true);
            PlacePlayer(gameState.playerPosition, gameState.playerRotation, gameState.cameraRotation);
            LogToConsole("Loaded game. Seed: " + gameState.seed.x + ", " + gameState.seed.y);
        }

        private void SaveGame()
        {
            // update state of current sectors
            int xMin = CurrentSectorOffset.x - sectors.GetLength(0) / 2;
            int zMin = CurrentSectorOffset.y - sectors.GetLength(1) / 2;
            int xMax = CurrentSectorOffset.x + sectors.GetLength(0) / 2;
            int zMax = CurrentSectorOffset.y + sectors.GetLength(1) / 2;
            int xMod = sectors.GetLength(0);
            int zMod = sectors.GetLength(1);

            for (int x = xMin; x <= xMax; x++)
            {
                int ix = (x % xMod + xMod) % xMod;
                for (int z = zMin; z <= zMax; z++)
                {
                    int iz = (z % zMod + zMod) % zMod;
                    sectors[ix, iz].SaveSectorState();
                }
            }

            gameState.PackChangedSectorWrapper();
            string gameStateSerialized = JsonUtility.ToJson(gameState);

            File.WriteAllText(SavefilePath, gameStateSerialized);

            LogToConsole("Saved game. Seed: " + gameState.seed.x + ", " + gameState.seed.y);
        }
        #endregion

        #region UI
        private void UpdateHints()
        {
            modeText.text = isPlacingMode ? "Place" : "Break";
        }
        #endregion

        #region Dynamic Generation
        private void ChangeSector(Vector2Int newSectorOffset, bool newGameOrLoading = false)
        {
            LogToConsole("Entered sector [" + newSectorOffset.x + ", " + newSectorOffset.y + "]");

            int xMin = newSectorOffset.x - sectors.GetLength(0) / 2;
            int zMin = newSectorOffset.y - sectors.GetLength(1) / 2;
            int xMax = newSectorOffset.x + sectors.GetLength(0) / 2;
            int zMax = newSectorOffset.y + sectors.GetLength(1) / 2;
            int xMod = sectors.GetLength(0);
            int zMod = sectors.GetLength(1);

            for (int x = xMin; x <= xMax; x++)
            {
                int ix = (x % xMod + xMod) % xMod;
                for (int z = zMin; z <= zMax; z++)
                {
                    int iz = (z % zMod + zMod) % zMod;
                    Vector2Int sectorOffset = new Vector2Int(x, z);
                    if (!(sectors[ix, iz].IsGenerated) || sectors[ix, iz].CurrentSectorOffset != sectorOffset || newGameOrLoading)
                    {
                        sectors[ix, iz].GenerateSector(gameState.SeedFloat, sectorOffset, true, gameState.GetSectorState(sectorOffset));
                    }
                }
            }

            CurrentSectorOffset = newSectorOffset;
        }

        public void SaveSectorState(Vector2Int sectorOffset, SectorState sectorState)
        {
            if (gameState.changedSectors == null)
            {
                gameState.changedSectors = new Dictionary<Vector2Int, SectorState>();
            }
            gameState.changedSectors[sectorOffset] = sectorState;
        }
        #endregion
        #endregion
    }
}