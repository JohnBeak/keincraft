﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Keincraft
{
    [Serializable]
    public class SectorState
    {
        #region Data
        [Serializable]
        public class TileWrapper
        {
            #region Data
            public Vector3Int key;
            public TileState value;
            #endregion

            #region Interface
            public TileWrapper(Vector3Int _key, TileState _value)
            {
                key = _key;
                value = _value;
            }
            #endregion
        }

        private bool _isChanged;

        // Unity serializer doesn't support Dictionary; packing data using List
        public Dictionary<Vector3Int, TileState> changedTiles;
        public List<TileWrapper> changedTileWrapper;
        #endregion

        #region Interface
        #region ...
        public bool IsChanged
        {
            get
            {
                return _isChanged;
            }
            set
            {
                _isChanged = value;
            }
        }
        #endregion

        #region Init
        public SectorState()
        {
            IsChanged = false;
            changedTiles = new Dictionary<Vector3Int, TileState>();
            changedTileWrapper = new List<TileWrapper>();
        }

        public SectorState(SectorState sectorStateToCopy)
        {
            IsChanged = sectorStateToCopy.IsChanged;
            changedTiles = sectorStateToCopy.changedTiles;
        }
        #endregion

        #region Serialization
        public void PackChangedTileWrapper()
        {
            if (changedTiles == null)
            {
                changedTiles = new Dictionary<Vector3Int, TileState>();
            }
            if (changedTileWrapper == null)
            {
                changedTileWrapper = new List<TileWrapper>();
            }

            changedTileWrapper.Clear();
            foreach (KeyValuePair<Vector3Int, TileState> changedTileKeyValuePair in changedTiles)
            {
                changedTileWrapper.Add(new TileWrapper(changedTileKeyValuePair.Key, changedTileKeyValuePair.Value));
            }
        }

        public void UnpackChangedTileWrapper()
        {
            if (changedTiles == null)
            {
                changedTiles = new Dictionary<Vector3Int, TileState>();
            }
            if (changedTileWrapper == null)
            {
                changedTileWrapper = new List<TileWrapper>();
            }

            changedTiles.Clear();
            foreach (TileWrapper tileWrapper in changedTileWrapper)
            {
                changedTiles.Add(tileWrapper.key, tileWrapper.value);
            }
        }
        #endregion
        #endregion
    }
}