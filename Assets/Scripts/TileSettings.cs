﻿using UnityEngine;

namespace Keincraft
{
    public class TileSettings : MonoBehaviour
    {
        #region Data
        [SerializeField]
        private TileType tileType;
        [SerializeField]
        private int maxHealth;
        [SerializeField]
        private Texture textureBasicLow;
        [SerializeField]
        private Texture textureBasic;
        [SerializeField]
        private Texture textureBasicHigh;
        [SerializeField]
        private Texture textureBricks;
        [SerializeField]
        private int layer = 9;
        public bool rendererEnabled = true;
        #endregion

        #region Interface
        public int MaxHealth
        {
            get
            {
                return maxHealth;
            }
        }

        public int Layer
        {
            get
            {
                return layer;
            }
        }

        public bool RendererEnabled
        {
            get
            {
                return rendererEnabled;
            }
        }

        public Texture GetTexture(TileHeight tileHeight)
        {
            switch (tileHeight)
            {
                case (TileHeight.Low):
                    return textureBasicLow;
                case (TileHeight.Mid):
                    return textureBasic;
                case (TileHeight.High):
                    return textureBasicHigh;
                case (TileHeight.Bricks):
                    return textureBricks;
            }
            return null;
        }
        #endregion
    }
}