﻿using System.Collections.Generic;
using UnityEngine;

namespace Keincraft
{
    public class Sector : MonoBehaviour
    {
        #region Data
        private static GameLogic GameLogicReference
        {
            get
            {
                if (_gameLogicReference == null)
                {
                    _gameLogicReference = FindObjectOfType<GameLogic>();
                }
                return _gameLogicReference;
            }
        }
        private static GameLogic _gameLogicReference;

        private Tile[,,] tiles;
        private Vector3Int dimensions = new Vector3Int(8, 32, 8); // 2048 tiles per sector
        private Vector2Int _currentSectorOffset;
        private bool _isGenerated = false;
        #endregion

        #region Interface
        #region Public properties
        public Vector2Int CurrentSectorOffset
        {
            get
            {
                return _currentSectorOffset;
            }
        }
        public bool IsGenerated
        {
            get
            {
                return _isGenerated;
            }
        }
        #endregion

        #region Dynamic Generation
        public void GenerateSector(Vector2 seed, Vector2Int newSectorOffset, bool newGameOrLoading, SectorState sectorState)
        {
            if (IsGenerated)
            {
                GameLogicReference.LogToConsole("Destroying Sector [" + CurrentSectorOffset.x + ", " + CurrentSectorOffset.y + "]");
                if (!newGameOrLoading)
                {
                    SaveSectorState();
                }
            }

            // Generate sector

            // repeat after 512 sectors
            // sector has 8 tiles in each direction
            // Mathf.PerlinNoise is poorly documented. Seems like it loops after 256 units.

            float step = 1f / 16f;
            float stoneHeight = 16f;
            float offsetScale = 1f / 2f;
            float stoneStepScale = 1f;
            int dirtHideHeight = 2;
            float dirtHeight = 8f;
            float dirtStepScale = 2f;
            int sandHideHeight = 4;
            float sandHeight = 16f;
            float sandStepScale = 1f / 2f;

            int yMaxStone = 0;
            float heightLimitBasedOnStoneHeight = 0;
            int yMaxDirt = 0;
            int yMaxSand = 0;

            for (int x = 0; x < dimensions.x; x++)
            {
                for (int z = 0; z < dimensions.z; z++)
                {
                    yMaxStone = 1 + (int)Mathf.Round(stoneHeight * Mathf.PerlinNoise(
                        ((seed.x + (float)newSectorOffset.x * offsetScale * stoneStepScale + (float)x * step * stoneStepScale) % 256f + 256f) % 256f,
                        ((seed.y + (float)newSectorOffset.y * offsetScale * stoneStepScale + (float)z * step * stoneStepScale % 256f) + 256f) % 256f));
                    heightLimitBasedOnStoneHeight = 1f - ((float)yMaxStone / stoneHeight);
                    yMaxDirt = yMaxStone - dirtHideHeight + (int)Mathf.Round(heightLimitBasedOnStoneHeight * dirtHeight * Mathf.PerlinNoise(
                        ((seed.x + (float)newSectorOffset.x * offsetScale * dirtStepScale + (float)x * step * dirtStepScale) % 256f + 256f) % 256f,
                        ((seed.y + (float)newSectorOffset.y * offsetScale * dirtStepScale + (float)z * step * dirtStepScale % 256f) + 256f) % 256f));
                    yMaxSand = yMaxStone - sandHideHeight + (int)Mathf.Round(heightLimitBasedOnStoneHeight * sandHeight * Mathf.PerlinNoise(
                        ((seed.x + (float)newSectorOffset.x * offsetScale * sandStepScale + (float)x * step * sandStepScale) % 256f + 256f) % 256f,
                        ((seed.y + (float)newSectorOffset.y * offsetScale * sandStepScale + (float)z * step * sandStepScale % 256f) + 256f) % 256f));
                    for (int y = 0; y < dimensions.y; y++)
                    {
                        Random.InitState((((newSectorOffset.x + x) * 7 + (newSectorOffset.y + z)) * 11 + y) + 13);

                        Random.Range(-3, 3); // empty RNG calls to keep generation identical to legacy version
                        Random.Range(-3, 3);

                        if (y < yMaxStone)
                        {
                            if (Random.Range(0, 255) == 0)
                            {
                                tiles[x, y, z].SetTile(TileType.Diamond, false);
                            }
                            else
                            {
                                tiles[x, y, z].SetTile(TileType.Stone, false);
                            }
                        }
                        else if (y < yMaxDirt)
                        {
                            tiles[x, y, z].SetTile(TileType.Dirt, false);
                        }
                        else if (y < yMaxSand)
                        {
                            tiles[x, y, z].SetTile(TileType.Sand);
                        }
                        else
                        {
                            tiles[x, y, z].SetTile(TileType.Air);
                        }
                    }
                }
            }

            _currentSectorOffset = newSectorOffset;
            transform.position = new Vector3(newSectorOffset.x * (float)dimensions.x, 0f, newSectorOffset.y * (float)dimensions.z);

            if (IsGenerated)
            {
                GameLogicReference.LogToConsole("Created Sector [" + newSectorOffset.x + ", " + newSectorOffset.y + "]");
            }

            _isGenerated = true;

            // Load changed tiles
            LoadSectorState(sectorState);
        }
        #endregion

        #region Serialization               
        public void SaveSectorState()
        {
            SectorState oldSectorState = new SectorState();

            for (int x = 0; x < dimensions.x; x++)
            {
                for (int z = 0; z < dimensions.z; z++)
                {
                    for (int y = 0; y < dimensions.y; y++)
                    {
                        if (tiles[x, y, z].TileStateIsChanged)
                        {
                            oldSectorState.IsChanged = true;
                            oldSectorState.changedTiles.Add(new Vector3Int(x, y, z), new TileState(tiles[x, y, z].CurrentTileState));
                        }
                    }
                }
            }

            if (oldSectorState.IsChanged)
            {
                GameLogicReference.SaveSectorState(CurrentSectorOffset, oldSectorState);
            }
        }

        public void LoadSectorState(SectorState sectorState)
        {
            if (sectorState == null)
            {
                return;
            }
            // TODO
            int x, y, z = 0;
            TileState tileState = null;

            foreach (KeyValuePair<Vector3Int, TileState> changedTile in sectorState.changedTiles)
            {
                x = changedTile.Key.x;
                y = changedTile.Key.y;
                z = changedTile.Key.z;
                tileState = changedTile.Value;
                tiles[x, y, z].SetTile(tileState);
            }
        }
        #endregion
        #endregion

        #region Implementation
        private void Awake()
        {
            tiles = new Tile[dimensions.x, dimensions.y, dimensions.z];
            int x = 0;
            int y = 0;
            int z = 0;
            Tile[] childTiles = GetComponentsInChildren<Tile>();
            foreach (Tile childTile in childTiles)
            {
                childTile.transform.localPosition = new Vector3(x - dimensions.x / 2f, y, z - dimensions.z / 2f);
                childTile.UpdateCoords();
                tiles[x, y, z] = childTile;

                z++;
                if (z >= dimensions.z)
                {
                    z = 0;
                    x++;
                    if (x >= dimensions.x)
                    {
                        x = 0;
                        y++;
                        if (y >= dimensions.y)
                        {
                            y = 0;
                        }
                    }
                }
            }
        }
        #endregion
    }
}