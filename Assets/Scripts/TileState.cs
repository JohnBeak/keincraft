﻿using System;

namespace Keincraft
{
    [Serializable]
    public class TileState
    {
        #region Data
        public bool isChanged;
        public int currentHealth;
        public TileType tileType;
        public bool isPlaced;
        #endregion

        #region Interface
        public TileState()
        {
            isChanged = false;
            currentHealth = 0;
            tileType = TileType.Air;
            isPlaced = false;
        }

        public TileState(TileState tileStateToCopy)
        {
            isChanged = tileStateToCopy.isChanged;
            currentHealth = tileStateToCopy.currentHealth;
            tileType = tileStateToCopy.tileType;
            isPlaced = tileStateToCopy.isPlaced;
        }
        #endregion
    }
}