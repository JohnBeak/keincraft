﻿namespace Keincraft
{
    public enum TileType
    {
        Air = 0,
        Stone = 1,
        Dirt = 2,
        Sand = 3,
        Diamond = 4
    }

    public enum TileHeight
    {
        Low,
        Mid,
        High,
        Bricks
    }
}